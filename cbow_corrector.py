# -*- coding: utf-8 -*-
"""
Created on Wed Sep 05 11:43:20 2021

@author: Administrator
"""

import pandas as pd
import torch
import jieba
from collections import defaultdict
from tqdm import tqdm
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader


class Vocab:
    pad_token = '<pad>'
    unk_token = '<unk>'
    bos_token = '<bos>'
    eos_token = '<eos>'

    def __init__(self, tokens=None):

        self.token2id = {}
        self.id2token = []

        super(Vocab, self).__init__()
        if tokens:
            tokens = ' '.join(tokens)
            if self.unk_token not in tokens:
                tokens.append(self.unk_token)

            self.id2token.extend(('<unk>', '<pad>', '<bos>', '<eos>'))
            self.token2id['<unk>'] = 0
            self.token2id['<pad>'] = 1
            self.token2id['<bos>'] = 2
            self.token2id['<eos>'] = 3

            for token in jieba.lcut(tokens):
                if token not in ['<', 'unk', '>', ' ', 'pad','bos' ,'eos']:#防止jieba.lcut把这些已有的标识符分割，
                    self.id2token.append(token)
                    self.token2id[token] = len(self.id2token) - 1

            self.unk_id = self.token2id[self.unk_token]
            self.pad_id = self.token2id[self.pad_token]

    def __len__(self):
        return len(self.id2token)

    def __getitem__(self, token):
        return self.token2id.get(token, self.unk_id)

    @classmethod
    def build(cls, corpus, min_freq=1, add_pad_token=False, add_eb_token=False):
        """
        构建vocab

        :param add_eb_token:        是否添加 <eos>、<bos>特殊符号, bool, 默认False
        :param corpus:              预料, char级别: list(str), word级别: list(list)
        :param min_freq:            vocab舍弃的最低频率
        :param add_pad_token:       是否添加pad特殊符号 <pad>, bool, 默认False
        :return:                    Vocab
        """

        # 计算频率
        token_freq = defaultdict(int)  # default dic默认值为0
        for seq in corpus:
            for token in jieba.lcut(seq):
                token_freq[token] += 1

        # 添加特殊符号
        vocab = [cls.unk_token, cls.pad_token] if add_pad_token else [cls.unk_token]
        if add_eb_token:
            vocab.extend([cls.bos_token, cls.eos_token])

        vocab += [token for token, freq in token_freq.items() if
                  freq >= min_freq and token not in [cls.unk_token, cls.pad_token]]

        return cls(vocab)

    def convert_tokens2ids(self, tokens):
        return [self[token] for token in jieba.lcut(tokens)]

    def convert_ids2tokens(self, ids):
        return [self.id2token[i] for i in ids]


class CbowDataset(Dataset):

    def __init__(self, src_seqs, vocab, cor_seqs=None, context_size=2):
        """
        基于词袋模型的纠错dataSet

        :param src_seqs:        源句, list(str)
        :param cor_seqs:        正确句(目标句), list(str)
        :param vocab:           词表: Vocab
        :param context_size:    上下文窗口
        """

        self.vocab = vocab
        self.src_seqs = src_seqs
        self.cor_seqs = cor_seqs
        self.context_size = context_size


    def __len__(self):
        return len(self.src_seqs)

    def __getitem__(self, idx):
        if self.cor_seqs is not None:
            return self.src_seqs[idx], self.cor_seqs[idx]
        else:
            return self.src_seqs[idx]

    def collect_fn(self, batch):
        """

        :param batch:
        :return:
        """

        labels, len_seqs_cor = [],[]
        if self.cor_seqs is not None:

            src_seqs, cor_seqs = zip(*batch)# 元组

            for cor_seq in cor_seqs:
                len_seqs_cor.append(len(self.vocab.convert_tokens2ids(tokens=cor_seq)))

            cor_seqs = ' '.join(cor_seqs)
            for cor_seq in jieba.lcut(cor_seqs):
                if cor_seq != ' ':
                    labels.extend(self.vocab.convert_tokens2ids(cor_seq))

        else:
            src_seqs = batch

        # --------------------------- 构造样本 start -----------------------------
        inputs ,len_seqs_src = [], []


        for src_seq in src_seqs:
            # tokens2ids, 在seq前补 <pad>、<bos>, 在seq后补 <bos>、<pad>
            new_seq = [self.vocab[self.vocab.pad_token]] * (self.context_size - 1) + \
                      [self.vocab[self.vocab.bos_token]] + \
                      self.vocab.convert_tokens2ids(tokens=src_seq) + \
                      [self.vocab[self.vocab.eos_token]] + \
                      [self.vocab[self.vocab.pad_token]] * (self.context_size - 1)

            seq_windows = [
                new_seq[idx - self.context_size: idx] + new_seq[idx + 1:idx + 1 + self.context_size]
                for idx in range(self.context_size, len(new_seq) - self.context_size)
            ]

            len_seqs_src.append(len(self.vocab.convert_tokens2ids(tokens = src_seq)))

            inputs.extend(seq_windows)


        # --------------------------- 构造样本 end -----------------------------

        if self.cor_seqs is not None:
            return src_seqs, inputs, labels, len_seqs_src,len_seqs_cor
        else:
            return inputs, len_seqs_src


class CBOWCorrector(nn.Module):

    def __init__(self, vocab_size, embedding_dim):
        super(CBOWCorrector, self).__init__()

        self.embeddings = nn.Embedding(vocab_size, embedding_dim)
        initrange = embedding_dim
        self.embeddings.weight.data.uniform_(-1/initrange, 1/initrange)

        self.output = nn.Linear(embedding_dim, vocab_size, bias=False)

    def forward(self, inputs):
        # inputs: [batch, num_window]
        outputs = self.embeddings(inputs)
        # outputs = self.hidden_ffn(outputs)
        outputs = self.output(torch.mean(outputs, dim=1))
        outputs = F.log_softmax(outputs, dim=-1)

        return outputs

# for test
# if __name__ == '__main__':
#     # reading data
#     val_df = pd.read_csv(r"C:\Users\Administrator\Desktop\corData\val.csv", encoding='utf-8', header=0, nrows=5,
#                          usecols=['src_seq', 'cor_seq'])
#
#     print(val_df.head())
#
#     corpus = val_df['cor_seq'].values.tolist() + val_df['src_seq'].values.tolist()
#
#     # building vocab
#     vocab = Vocab.build(corpus, min_freq=1, add_pad_token=True, add_eb_token=True)  # 字对应字表
#     dataset = CbowDataset(src_seqs=val_df['src_seq'].values.tolist(), cor_seqs=val_df['cor_seq'].values.tolist(),
#                           vocab=vocab)
#
#     data_loader = DataLoader(dataset=dataset, batch_size=2, shuffle=False, num_workers=4,
#                              collate_fn=dataset.collect_fn)
#     device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
#     # define model
#     model = CBOWCorrector(vocab_size=vocab.__len__(), embedding_dim=300)
#     model = model.to(device)
#     loss_function = nn.NLLLoss()
#     for src_seqs, inputs, labels, seq_lens in tqdm(data_loader):
#
#         # put data to device
#         #inputs = torch.tensor(inputs, dtype=torch.long, device=device)
#         for i in range(len(inputs)):
#
#             decoded_inputs = vocab.convert_ids2tokens(inputs[i])
#             print(decoded_inputs)
#         labels = torch.tensor(labels, dtype=torch.long, device=device)
#
#         outputs = model(inputs)
#         # batch_loss = [loss_function(output, label) for output, label in zip(outputs, labels)]
#         # loss = sum(batch_loss)
#         print(seq_lens)
#         print(outputs)
#         print(outputs.size())
