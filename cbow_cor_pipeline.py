import argparse
import time

import os
import pandas as pd
import numpy as np
from tqdm import tqdm

import torch
import torch.nn as nn
from torch.utils.data import DataLoader

from cbow_corrector1 import CbowDataset, CBOWCorrector, Vocab


def set_all_seed(seed):
    """设置随机种子 """
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    torch.backends.cudnn.deterministic = True


class EarlyStopping:
    """早停"""

    def __init__(self, early_stop_rounds, model_path):
        self.early_stop_rounds = early_stop_rounds
        self.model_path = model_path

        self.counter = 0
        self.best_loss = float('inf')
        self.early_stop = False

    def __call__(self, val_loss, model):
        if val_loss > self.best_loss:
            self.counter += 1
            if self.counter >= self.early_stop_rounds:
                self.early_stop = True
        else:
            self.counter = 0
            self.best_loss = val_loss
            torch.save(model.state_dict(), self.model_path)


class CbowCorPipeline:

    def __init__(self, model, args):
        """

        :param model:           cbow模型
        :param args:            参数
        """

        super(CbowCorPipeline, self).__init__()

        self.model = model
        self.args = args

        # 多机多卡
        if self.args.n_gpus == 1:
            self.model = nn.DataParallel(model)
        self.model = model.to(args.device)

    def prediction_processor(self, outputs, labels, seq_lens):
        """
        预测结果后处理

        :param seq_lens:        每个句子的长(list(int))
        :param outputs:         list(tensor(seq_len, vocab_size))
        :param labels:          list(tensor)
        :return:
        """

        # tensor(batch_size * sum_seq_len, )
        pres = torch.argmax(outputs, dim=-1).view(-1)

        pre_label_pairs = []
        start_idx = 0
        for seq_len in seq_lens:
            pre_seq = ''.join(self.args.token_vocab.convert_ids2tokens(pres[start_idx: start_idx + seq_len]))
            label_seq = ''.join(self.args.token_vocab.convert_ids2tokens(labels[start_idx: start_idx + seq_len]))
            pre_label_pairs.append((pre_seq, label_seq))
            start_idx += seq_len

        acc_count = sum(1 if pre == label else 0 for pre, label in pre_label_pairs)

        return acc_count, pre_label_pairs

    def validate(self, data_loader, return_pres=False):
        """

        :param return_pres:
        :param data_loader:
        :return:
        """

        loss_function = nn.NLLLoss()
        val_loss, val_cor_acc, n = 0.0, 0, 0
        res = []
        self.model.eval()
        with torch.no_grad():
            for src_seqs, inputs, labels, seq_lens_src, seq_lens_cor in tqdm(data_loader):
                # put labels to device
                inputs = torch.tensor(inputs, dtype=torch.long, device=self.args.device)
                labels = torch.tensor(labels, dtype=torch.long, device=self.args.device)

                # outputs: list(tensor(seq_len, vocab_size))
                outputs = self.model(inputs)

                # computing loss

                start_idx_src, start_idx_cor, loss_list = 0, 0, []
                for seq_len_src,seq_len_cor in zip(seq_lens_src,seq_lens_cor):
                    seq_loss = loss_function(outputs[start_idx_src:start_idx_src + seq_len_src],
                                             labels[start_idx_cor:start_idx_cor + seq_len_src])
                    loss_list.append(seq_loss)
                    val_loss += seq_loss.item()
                    start_idx_src += seq_len_src
                    start_idx_cor += seq_len_cor

                acc_count, pre_label_pairs = self.prediction_processor(outputs=outputs, labels=labels,
                                                                       seq_lens=seq_lens_cor)

                n += len(labels)
                val_cor_acc += acc_count

                if return_pres:
                    res.extend([(src_seq, pair[1], pair[0]) for src_seq, pair in zip(src_seqs, pre_label_pairs)])

            val_loss /= n
            val_cor_acc /= n
        if return_pres:
            return val_loss, val_cor_acc, res
        else:
            return val_loss, val_cor_acc

    def predict(self, seqs):
        """
        模型预测

        :param seqs:            list(str)
        :return:                list(str)
        """

        self.model.eval()
        test_dataset = CbowDataset(src_seqs=seqs, vocab=self.args.token_vocab, context_size=self.args.context_size)
        test_data_loader = DataLoader(dataset=test_dataset, batch_size=self.args.batch_size, shuffle=False,
                                      num_workers=self.args.num_workers, collate_fn=test_dataset.collect_fn)

        predictions = []

        with torch.no_grad():
            for inputs, seq_lens in tqdm(test_data_loader):
                # put labels to device
                inputs = torch.tensor(inputs, dtype=torch.long, device=self.args.device)
                # outputs: list(tensor(seq_len, vocab_size))
                outputs = self.model(inputs)
                # tensor(batch_size * sum_seq_len, )
                pres = torch.argmax(outputs, dim=-1).view(-1)

                start_idx = 0
                for seq_len in seq_lens:
                    pre_seq = ''.join(self.args.token_vocab.convert_ids2tokens(pres[start_idx: start_idx + seq_len]))
                    predictions.append(pre_seq)
                    start_idx += seq_len

        return predictions

    def train(self, trn_df, val_df):
        """
        :return:        验证集预测结果: DataFrame(src_seq(str), cor_pre_seq(str)]
        """

        # 定义dataLoader
        train_dataset = CbowDataset(src_seqs=trn_df['src_seq'].values, cor_seqs=trn_df['cor_seq'].values,
                                    vocab=self.args.token_vocab, context_size=self.args.context_size)
        train_data_loader = DataLoader(dataset=train_dataset, batch_size=self.args.batch_size, shuffle=False,
                                       num_workers=self.args.num_workers, collate_fn=train_dataset.collect_fn)
        val_dataset = CbowDataset(src_seqs=val_df['src_seq'].values, cor_seqs=val_df['cor_seq'].values,
                                  vocab=self.args.token_vocab, context_size=self.args.context_size)
        val_data_loader = DataLoader(dataset=val_dataset, batch_size=self.args.batch_size, shuffle=False,
                                     num_workers=self.args.num_workers, collate_fn=val_dataset.collect_fn)

        start_time = time.time()
        optimizer = torch.optim.Adam(self.model.parameters(), lr=self.args.lr)
        early_stop = EarlyStopping(early_stop_rounds=self.args.early_stop_rounds, model_path=args.check_point_path)
        loss_function = nn.NLLLoss()

        # start training
        for epoch in range(1, self.args.num_epochs + 1):

            self.model.train()
            trn_loss, n, trn_cor_acc = 0.0, 0, 0

            for src_seqs, inputs, labels, seq_lens_src,seq_lens_cor in tqdm(train_data_loader, desc='[epoch {:02d}/{:02d}]'.format(epoch,
                                                                                                                  self.args.num_epochs)):
                # put data to device
                inputs = torch.tensor(inputs, dtype=torch.long, device=self.args.device)
                labels = torch.tensor(labels, dtype=torch.long, device=self.args.device)

                # outputs: tensor(batch_size * sum_seq_len, vocab_size)
                outputs = self.model(inputs)

                # computing loss

                start_idx_src, start_idx_cor, loss_list = 0, 0, []
                for seq_len_src,seq_len_cor in zip(seq_lens_src,seq_lens_cor):
                    seq_loss = loss_function(outputs[start_idx_src:start_idx_src + seq_len_src], labels[start_idx_cor:start_idx_cor + seq_len_src])
                    loss_list.append(seq_loss)
                    trn_loss += seq_loss.item()
                    start_idx_src += seq_len_src
                    start_idx_cor += seq_len_cor

                # 梯度清零
                optimizer.zero_grad()

                # 反向传播
                loss = sum(loss_list)
                loss.backward()

                # 更新参数
                optimizer.step()

                with torch.no_grad():
                    n += len(labels)
                    cor_acc_count, pre_label_pairs = self.prediction_processor(outputs, labels, seq_lens_cor)
                    trn_cor_acc += cor_acc_count

            # 训练集指标
            trn_loss /= n
            trn_cor_acc /= n
            print('[epoch {:02d}/{:02d}] | trn_loss {:.5f} | trn_cor_acc {:.5f}'.format(epoch, self.args.num_epochs,
                                                                                        trn_loss, trn_cor_acc))

            # 验证集指标
            val_loss, val_cor_acc = self.validate(data_loader=val_data_loader, return_pres=False)
            print('[epoch {:02d}/{:02d}] | val_loss {:.5f} | val_cor_acc {:.5f}'.format(epoch, self.args.num_epochs,
                                                                                        val_loss, val_cor_acc))
            # 早停
            early_stop(val_loss, self.model)
            if early_stop.early_stop:
                break

        # 加载最好的模型进行最后的验证
        self.model.load_state_dict(torch.load(early_stop.model_path))
        val_loss, val_cor_acc, val_res = self.validate(data_loader=val_data_loader, return_pres=True)

        # 将验证集的纠错结果进行保存
        val_pre_df = pd.DataFrame(val_res)
        val_pre_df.columns = ['src_seq', 'cor_seq', 'pre_seq']
        val_pre_df.to_csv(self.args.val_pre_save_path, index=False, encoding='utf-8', )

        print('finish training, cost {:.2f} min. | val_loss {:.5f} | val_cor_acc {:.5f}'.format(
            (time.time() - start_time) / 60, val_loss, val_cor_acc))

        return val_loss, val_cor_acc, val_pre_df


if __name__ == '__main__':
    set_all_seed(20220910)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    trn_df = pd.read_csv('path', encoding='utf-8', nrows=10)
    print(trn_df.head())
    val_df = pd.read_csv('path', encoding='utf-8', nrows=10)
    test_df = pd.read_csv('path', encoding='utf-8', nrows=10)
    corpus= trn_df['cor_seq'].values.tolist()+trn_df['scr_seq'].values.tolist()+val_df['cor_seq'].values.tolist()+val_df['src_seq'].values.tolist()+ \
            test_df['cor_seq'].values.tolist()+test_df['src_seq'].values.tolist()

    # building vocab
    token_vocab = Vocab.build(corpus, min_freq=100, add_pad_token=True, add_eb_token=True)
    #pinyin_vocab = Vocab.build
    print(token_vocab.__len__())

    parser = argparse.ArgumentParser()
    parser.add_argument('--num_workers', default=4, type=int)
    parser.add_argument('--context_size', default=2, type=int)
    parser.add_argument('--n_gpus', default=1, type=int)
    parser.add_argument('--batch_size', default=5, type=int)
    parser.add_argument('--lr', default=1e-2, type=float)
    parser.add_argument('--embedding_dim', default=150, type=int)
    parser.add_argument('--hidden_size', default=128, type=int)
    parser.add_argument('--num_epochs', default=10, type=int)
    parser.add_argument('--early_stop_rounds', default=2, type=int)
    parser.add_argument('--check_point_path', default=r'./cbow_cor.pt', type=str)
    parser.add_argument('--val_pre_save_path', default=r'./val_pre.csv', type=str)
    parser.add_argument('--device', default=device, type=torch.device)
    parser.add_argument('--token_vocab', default=token_vocab, type=Vocab)
    args, unknown = parser.parse_known_args()

    model = CBOWCorrector(vocab_size=token_vocab.__len__(), embedding_dim=args.embedding_dim)
    cbow_pipeline = CbowCorPipeline(model=model, args=args)
    val_loss, val_cor_acc, val_pre_df = cbow_pipeline.train(trn_df=trn_df, val_df=val_df)
    print(f'val_loss: {val_loss}, val_cor_acc: {val_cor_acc}')
    print(val_pre_df.head(10))

    trn_df['pre_seq'] = cbow_pipeline.predict(trn_df['src_seq'].values.tolist())
    print(trn_df[['src_seq', 'pre_seq']].head(20))
    #test_df['pre_seq'].to_csv(r'./cbow_corrector/test_pre.csv', encoding='utf-8', index=False)
